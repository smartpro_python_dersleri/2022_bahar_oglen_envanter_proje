from datetime import datetime
from json import loads, dumps, JSONDecodeError


def create_log(type: str, job: str, desc: str) -> None:
    now = datetime.now()
    with open('log.txt', 'a') as file:
        new_log = f'{type} - {now} => {job} - {desc}\n'
        file.write(new_log)


def read_from_file() -> list[dict]:
    try:
        with open('item_database.json') as file:
            return loads(file.read())
    except FileNotFoundError:
        print('File does not exists, creating an empty one!')
        with open('item_database.json', 'w') as file:
            file.write(dumps([]))
        return []
    except JSONDecodeError:
        print('Database is corrupted! Please fix it!!!!')
        exit()


def write_to_file(data: list[dict]) -> bool:
    try:
        with open('item_database.json', 'w') as file:
            file.write(dumps(data, indent=2))
        return True
    except Exception as e:
        print("There is an error while writing to database!")
        print(e, e.args)
        return False
