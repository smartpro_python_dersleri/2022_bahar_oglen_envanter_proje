from get_input import get_str_input, get_float_input
from file_utils import create_log


class Item:
    __slots__ = ('name', 'price', 'stock', 'shelf')

    def __init__(self, name: str, price: float, stock: int, shelf: str) -> None:
        self.name = name
        self.price = price
        self.stock = stock
        self.shelf = shelf

    def __str__(self) -> str:
        return self.name

    def pretty_print(self) -> str:
        return f"{self.name} - {self.price} - {self.stock} - {self.shelf}"

    @classmethod
    def new(cls) -> 'Item':
        name = get_str_input('Name of the item')
        price = get_float_input('Price of the item')
        stock = int(get_float_input('Stock of the item'))
        shelf = get_str_input('Shelf id of the item')
        create_log('ADD', 'item', name)
        return cls(name, price, stock, shelf)

    @property
    def in_stock(self) -> bool:
        if self.stock > 0:
            return True
        return False

    def sell(self, num: int = 1) -> bool:
        if self.stock >= num:
            self.stock -= num
            create_log('SUB', 'Sale', f'{self.name} - {num}')
            return True
        return False

    def restock(self, num: int) -> int:
        self.stock += num
        create_log('RES', 'Restock', f'{self.name} - {num}')
        return self.stock

    def return_to_origin(self, num: int) -> bool:
        if self.stock >= num:
            self.stock -= num
            create_log('SUB', 'Return', f'{self.name} - {num}')
            return True
        return False

    def to_json(self) -> dict:
        return {
            'name': self.name,
            'price': self.price,
            'stock': self.stock,
            'shelf': self.shelf,
        }
