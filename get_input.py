def get_str_input(text: str) -> str:
    user_input = input(text + ': ')
    return user_input


def get_float_input(text: str) -> float:
    user_input = get_str_input(text)
    try:
        return float(user_input)
    except ValueError:
        print('Please provide a number!')
        return get_float_input(text)


def get_task_input(text: str, tasks: list[str]) -> str:
    print(tasks)
    user_input = get_str_input(text)

    if user_input.lower() in tasks:
        return user_input
    else:
        print('Please provide a correct command to run!')
        return get_task_input(text, tasks)
