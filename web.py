from flask import Flask, request

from main import ItemManager
from models import Item


app = Flask(__name__)


manager = ItemManager()


@app.route('/')
def list_items() -> list[dict]:
    return [item.to_json() for item in manager.items]


@app.route('/add', methods=['POST'])
def add_item() -> dict:
    try:
        item = Item(
            request.form['name'],
            float(request.form['price']),
            int(request.form['stock']),
            request.form['shelf']
        )
        manager.items.append(item)
        manager.save_to_db()
        return item.to_json()
    except Exception as e:
        raise e


app.run(debug=True, use_reloader=True)
